using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSphere : MonoBehaviour
{
    //GameObject sphere;
    //// Start is called before the first frame update
    //void Start()
    //{
    //    sphere = GameObject.Find("Sphere");
    //}

    // Update is called once per frame
    void Update()
    {
        //transform.localScale = new Vector3(10, 10, 10);
        //transform.position = new Vector3(10, 0, 10);

        if (Input.GetKeyDown(KeyCode.RightArrow))
            GetComponent<Rigidbody>().AddForce(new Vector3(3, 0, 0), ForceMode.Impulse);
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            GetComponent<Rigidbody>().AddForce(new Vector3(-3, 0, 0), ForceMode.Impulse);

        if (transform.position.y < -2.0f)
            transform.position = new Vector3(0, 0, 0);
    }
}
